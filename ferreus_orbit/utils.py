#!/usr/bin/python3

import os


def selectFromDict(options: dict, name: str) -> object:
    """
    Load information from a json file

    Parameters:
        options (dict): dict of options
        name (str): Description of selection

    Returns:
        object: the selection
    """
    index = 0
    indexValidList = []
    print('Select a ' + name + ':')
    for optionName in options:
        index = index + 1
        indexValidList.extend([options[optionName]])
        print(str(index) + ') ' + optionName)
    inputValid = False
    while not inputValid:
        inputRaw = input(name + ': ')
        inputNo = int(inputRaw) - 1
        if inputNo > -1 and inputNo < len(indexValidList):
            selected = indexValidList[inputNo]
            str_selected = str(selected)
            print(f"Selected {name}: {str_selected}")
            inputValid = True
            break
        else:
            print('Please select a valid ' + name + ' number')
    return selected


def bytes_to_human_readable(bytes: int) -> str:
    """
    Transform bytes to human readable size

    Parameters:
        bytes (int): the size in bytes

    Returns:
        str: human readable size
    """
    if not isinstance(bytes, int) or bytes <= 0:
        raise ValueError("input must be a positive integer")
    units = ["B", "KB", "MB", "GB", "TB", "PB"]
    index = 0
    size = bytes
    while size >= 1000 and index < len(units) - 1:
        size /= 1000
        index += 1
    return f"{int(size)}{units[index]}"


def clear_kern_legacy():
    """
    Clean the kernel legacy log
    """
    if os.path.exists("/var/log/kern-legacy.log"):
        with open("/var/log/kern-legacy.log", 'w') as f:
            f.write('')
