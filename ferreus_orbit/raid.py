#!/usr/bin/python3

import os
import subprocess
import json
from ferreus_orbit.systemtool import SystemTool
from ferreus_orbit.config import Config

conf = Config()


class Raid(SystemTool):
    """
    Class for retrieve informations by raid (storcli64 or ssacli) command
    """

    def run(self) -> list:
        """
        Get informations by raid (storcli64 or ssacli) command

        Returns:
            list: list of drive
        """
        self.logger.info("Retrieve Storcli infos")
        if os.path.exists("/usr/bin/storcli64"):
            Raid.cmd = "/usr/bin/storcli64"
        elif os.path.exists("/usr/bin/storcli_x64"):
            Raid.cmd = "/usr/bin/storcli_x64"
        elif os.path.exists("/usr/sbin/ssacli"):
            Raid.cmd = "/usr/sbin/ssacli"
            return self.load_ssacli()
        else:
            self.logger.warning("No raid command found")
            return []

        # Retrieve Controller
        cmd = [Raid.cmd, "/c0", "show", "J"]
        try:
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=120)
            data = json.loads(result.stdout)
            self.raid_card = data['Controllers'][0]['Response Data']['Product Name']
        except Exception as e:
            self.logger.error(f"Storcli to retrieve controllers error: {e}")
            Raid.cmd = "/usr/sbin/ssacli"
            return self.load_ssacli()
        return self.load_storcli()

    def load_storcli(self) -> list:
        """
        Get informations by storcli64 command

        Returns:
            list: list of drive
        """
        raid_cards = conf.get('raid_cards')
        if self.raid_card in raid_cards and 'path_all' in raid_cards[self.raid_card]:
            raid_path_all = raid_cards[self.raid_card]['path_all']
        else:
            raid_path_all = '/call/eall/sall'
        # Retrieve informations off all slot
        cmd = [Raid.cmd, raid_path_all, "show", "all", "J"]
        try:
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=120)
            self.data_all = json.loads(result.stdout)
        except Exception as e:
            self.logger.critical(f"Storcli to retrieve all infos error: {e}")
            return []
        # Retrtieve informations of virtual devices
        cmd = [Raid.cmd, "/call/vall", "show", "all", "J"]
        vd = {}
        try:
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=1200)
            data = json.loads(result.stdout)
            for controller in data['Controllers']:
                ctl = controller['Command Status']['Controller']
                response_data = controller['Response Data']
                for key, value in response_data.items():
                    if key.startswith('/c'):
                        vd[key.replace(f"/c{ctl}/v", '')] = {}
                    elif key.startswith('PDs for VD '):
                        slot = f"/c{ctl}/e{value[0]['EID:Slt'].replace(':', '/s')}"
                        vd[key.replace('PDs for VD ', '')]['slot'] = slot
                    elif key.startswith('VD'):
                        vdid = key.split()[0].replace('VD', '')
                        vd[vdid]['wwn'] = f"0x{value['SCSI NAA Id']}"
        except Exception as e:
            self.logger.info(f"Storcli to retrieve VD error : {e}")

        self.raid0_wwn = {}
        for value in vd.values():
            self.raid0_wwn[value['slot']] = value['wwn']
        drives = []
        for controller in self.data_all['Controllers']:
            response_data = controller['Response Data']
            iterator = iter(response_data.items())
            for key, value in iterator:
                keys = key.split()
                if f"{keys[0]} {keys[1]}" == key:
                    slot = keys[1]
                    state = value[0]["State"]
                    key, value = next(iterator)
                    serial = value[f"Drive {slot} Device attributes"]['SN']
                    vendor = value[f"Drive {slot} Device attributes"]['Manufacturer Id']
                    model = value[f"Drive {slot} Device attributes"]['Model Number']
                    rev = value[f"Drive {slot} Device attributes"]['Firmware Revision']
                    if slot in self.raid0_wwn:
                        wwn = self.raid0_wwn[slot]
                        type = "RAID0"
                    else:
                        wwn = value[f"Drive {slot} Device attributes"]['WWN']
                        raid_cards = conf.get('raid_cards')
                        if self.raid_card in raid_cards and 'transform_wwn' in raid_cards[self.raid_card]:
                            iwwn = int(wwn, 16)
                            iwwn = iwwn - raid_cards[self.raid_card]['transform_wwn']
                            wwn = hex(iwwn).lower()
                        else:
                            wwn = f"0x{wwn.lower()}"
                        type = "JBOD"
                    drives.append({'slot': slot,
                                   'serial': serial.strip(),
                                   'vendor': vendor.strip(),
                                   'model': model.strip(),
                                   'rev': rev.strip(),
                                   'wwn': wwn,
                                   'state': state,
                                   'type': type
                                   })
        return drives

    def load_ssacli(self) -> list:
        """
        Get informations by raid ssacli command

        Returns:
            list: list of drive
        """
        drives = []
        try:
            cmd = [Raid.cmd, "ctrl", "all", "show", "detail"]
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=120)
            for line in result.stdout.split('\n'):
                elem = line.split(':')
                if elem[0].strip().lower() == 'slot':
                    self.ctrl_slot = elem[1].strip()
            self.raid_card = "ssacli"
            cmd = [Raid.cmd, "ctrl", f"slot={self.ctrl_slot}", 'pd', 'all', "show", "detail"]
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=120)
            slot, serial, vendor, model, rev, wwn, state, type = '', '', '', '', '', '', '', ''
            for line in result.stdout.split('\n'):
                if line.strip().startswith("physicaldrive"):
                    if slot != '':
                        drives.append({'slot': slot,
                                       'serial': serial,
                                       'vendor': vendor,
                                       'model': model,
                                       'rev': rev,
                                       'wwn': f"0x{wwn.lower()}",
                                       'state': state,
                                       'type': type
                                       })
                        slot, serial, vendor, model, rev, wwn, state, type = '', '', '', '', '', '', '', ''
                    slot = line.split()[1]
                else:
                    elem = line.split(':')
                    if len(elem) > 1:
                        key = elem[0].strip().lower()
                        value = elem[1].strip()
                        if key == "status":
                            state = value
                        elif key == "serial number":
                            serial = value
                        elif key == "model":
                            values = value.split()
                            vendor = values[0].strip()
                            model = values[1].strip()
                        elif key == "firmware revision":
                            rev = value
                        elif key == "wwid":
                            wwn = value
            drives.append({'slot': slot,
                           'serial': serial,
                           'vendor': vendor,
                           'model': model,
                           'rev': rev,
                           'wwn': f"0x{wwn.lower()}",
                           'state': state,
                           'type': type
                           })
            return drives
        except Exception as e:
            self.logger.error(f"ssacli to retrieve controllers error: {e}")
            return []

    def update_firmware(self, path: str, fw: str, mode: str = '') -> bool:
        """
        Parameters:
            fw (str): filename of firmware
            mode (str): mode of update (see storcli documentation)

        Returns:
            bool: True if update is OK
        """
        if not hasattr(self, 'raid_card') or self.raid_card == 'ssacli':
            return False
        txt = "Normal mode" if mode else f"mode {mode}"
        self.logger.warning(f"Try update firmware with storcli in {txt}")
        cmd = [Raid.cmd,
               path,
               'download',
               f"src={conf.get('fw_dir')}/{fw}"]
        if mode:
            cmd.append(f"mode={mode}")
        try:
            subprocess.run(cmd,
                           stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL,
                           capture_output=False,
                           check=True, timeout=120)
            print("Update of Firmware OK")
            return True
        except Exception as e:
            self.logger.error(f"Update firmware for {path} in {txt} failed !")
            self.logger.error(e)
            return False

    def configure_raid0(self, path: str) -> bool:
        """
        Configure a VD in Raid 0 with the physical drive in controller, enclosure and slot

        Parameters:
            path (str): storcli path of device to configure

        Returns:
            bool: True Raid correctly created
        """
        if not hasattr(self, 'raid_card') or self.raid_card == 'ssacli':
            return False
        controller = ''
        enclosure = ''
        slot = ''
        for i in path.split('/'):
            if i.startswith('c'):
                controller = i[1:]
            elif i.startswith('e'):
                enclosure = i[1:]
            elif i.startswith('s'):
                slot = i[1:]
        cmd = [Raid.cmd, f"/c{controller}/vall", "delete", "preservedCache"]
        subprocess.run(cmd, timeout=30)
        cmd = [Raid.cmd, path, "show", "J"]
        try:
            result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=120)
            data = json.loads(result.stdout)
            state = data['Controllers'][0]['Response Data']['Drive Information'][0]["State"]
            if state == 'UGood':
                cmd = [Raid.cmd, f"{path}", "set", "good", "force"]
                subprocess.run(cmd, timeout=30)
                cmd = [Raid.cmd, f"/c{controller}", "add", "vd", "r0", f"drives=\"{enclosure}:{slot}\"", "wb", "ra"]
                try:
                    subprocess.run(cmd, check=True, timeout=30)
                except Exception:
                    cmd = [Raid.cmd, f"/c{controller}", "add", "vd", "r0", f"drives=\"{enclosure}:{slot}\"", "wb", "ra"]
                    try:
                        subprocess.run(cmd, check=True, timeout=30)
                        return True
                    except Exception as e:
                        print(f"Configure Raid0 for physical drive {path} failed")
                        self.logger.error(e)
            else:
                print(f"Physical drive {path} not in UGood state")
                return False
        except Exception as e:
            print(e)
            self.logger.error(e)
            return False

    def get_state(self, path: str) -> str:
        """
        Get state of device

        Parameters:
            path (str): storcli path of device to configure

        Returns:
            str: State of device
        """
        if path:
            device = self.get(slot=path)
            return f"{device['type']} {device['state']}"
        else:
            return ""

    def locate(self, path: str, locate: bool):
        """
        Locate or unlocate a device

        Parameters:
            path (str): raid path of device to configure
            locate: start or stop locate
        """
        if self.raid_card == 'ssacli':
            cmd = [Raid.cmd, 'ctrl', f"slot={self.ctrl_slot}", 'pd', path, 'modify', f"led={'on' if locate else 'off'}"]
        else:
            cmd = [Raid.cmd, path, 'start' if locate else 'stop', 'locate']
        try:
            subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except Exception as e:
            self.logger.warning(f"Locate error: {e}")
