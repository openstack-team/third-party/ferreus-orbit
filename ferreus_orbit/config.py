#!/usr/bin/python3

import yaml


class Config:
    """
    Configuration off this app
    """

    def __init__(self):
        """
        Load configuration
        """
        with open("/etc/ferreus-orbit/config.yaml", 'r') as f:
            self.config = yaml.load(f, Loader=yaml.SafeLoader)

    def get(self, key):
        """
        Get a configuration key
        """
        return self.config[key]

