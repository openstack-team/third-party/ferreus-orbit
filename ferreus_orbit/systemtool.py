#!/usr/bin/python3

from abc import ABC, abstractmethod
import logging


class SystemTool(ABC):
    """
    Abstract class
    """

    def __init__(self, level: int = logging.CRITICAL):
        """
        Run run method in subclass and affect returned list to entries
        """
        self.logger = logging.getLogger(f"ferreus_orbit_{self.__class__.__name__}")
        self.logger.setLevel(level)
        self.entries = self.run()

    @abstractmethod
    def run(self) -> list:
        """
        Meant to be implemented in parent classes

        Returns:
            list
        """
        return []

    def get(self, **kwargs) -> dict:
        """
        Return one item from entries matching arbitrary keyword arguments

        Parameters:
            key=key_value

        Returns:
            disct: dict for entry key_value
        """
        lookup = []
        result = {}
        for item in self.entries:
            if any(item.get(key) == value for key, value in kwargs.items()):
                lookup.append(item)
        if not lookup:
            self.logger.warning(f"{self.__class__.__name__} found no entry matching '{kwargs}'")
        elif len(lookup) > 1:
            self.logger.warning(f"{self.__class__.__name__} found more than 1 entry matching '{kwargs}', skipping: {lookup}")
        else:
            result = lookup[0]
        return result

    def detail(self) -> list:
        """
        Return self.entries contents

        Returns:
            dict: List self.entries
        """
        return self.entries
