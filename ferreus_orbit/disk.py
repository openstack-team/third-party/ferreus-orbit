#!/usr/bin/python3
# PYTHON_ARGCOMPLETE_OK

import os
import argparse
import logging
import yaml
import json
import csv
import subprocess
import prettytable
import sys
import time
import pwd
import datetime
from ferreus_orbit.config import Config
from ferreus_orbit.fstab import FStab
from ferreus_orbit.lsblk import LSblk
from ferreus_orbit.lshw import LShw
from ferreus_orbit.raid import Raid
from ferreus_orbit.swiftrecon import SwiftRecon
from ferreus_orbit.utils import bytes_to_human_readable, selectFromDict, clear_kern_legacy

try:
    import argcomplete
except Exception:
    pass


class Disk(dict):
    """
    Class for control disk
    """
    field_names = [
        'mountpoint',
        'device',
        'size',
        'vendor',
        'model',
        'fw',
        'slot',
        'serial',
        'uuid',
        'wwn',
        'state',
        'mounted']
    list = []
    fstab = None
    lsblk = None
    lshw = None
    raid = None
    recon = None

    def __init__(self, *args, **kwargs):
        """
        Init a disk with default value and update it with *args if necessary
        """
        default = {}
        for f in [d for d in Disk.field_names if d != 'mounted']:
            default[f] = ''
        default['mounted'] = False
        default.update(*args)
        super().__init__(default, **kwargs)
        self.__dict__ = self
        Disk.list.append(self)

    def get_fildered(self) -> dict:
        """
        Return a dict with filtered keys

        Returns:
            dict: dict of keys filtered
        """
        return {key: value for (key, value) in dict(self).items() if key in Disk.field_names}

    def print(self, format: str = None):
        """
        Print disk informations

        Parameters:
            format (str): Output format
        """
        colors = conf.get('colors')
        out = self.get_fildered()
        if format == 'json':
            print(json.dumps(out, indent=4))
        elif format == 'yaml':
            print(yaml.dump(out, sort_keys=False))
        elif format == 'csv':
            writer = csv.DictWriter(sys.stdout, fieldnames=Disk.field_names)
            writer.writeheader()
            writer.writerows([out])
        elif format == 'textile':
            print()
            for key, value in out.items():
                print(f"|{key.upper()}|{value}|")
            print()
        else:
            if not self.mounted:
                print(f"{colors['term']['red']}")
            for key, value in out.items():
                print(f"{key.upper()}:\t{value}".expandtabs(12))
            print(f"{colors['term']['neutral']}")

    def locate(self, format: str = None):
        """
        Active disk location LED and print "Locate ON"

        Parameters:
            format (str): Output format
        """
        if format == 'textile':
            print("*_Locate ON_*")
        elif format is None:
            print("Locate ON")
        if self.slot:
            if Disk.raid is None:
                Disk.raid = Raid()
            Disk.raid.locate(self.slot, True)
        else:
            cmd = ["ledctl", f"locate={self.device}"]
            try:
                subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            except Exception as e:
                logger.warning(f"Locate error: {e}")

    def unlocate(self, format: str = None):
        """
        Unactive disk location LED and print "Locate OFF"

        Parameters:
            format (str): Output format
        """
        if format == 'textile':
            print("*_Locate OFF-*")
        elif format is None:
            print("Locate OFF")
        if self.slot:
            if Disk.raid is None:
                Disk.raid = Raid()
            Disk.raid.locate(self.slot, False)
        else:
            cmd = ["ledctl", f"locate_off={self.device}"]
            try:
                subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            except Exception as e:
                logger.warning(f"Unlocate error: {e}")

    def load(self):
        """
        Load disk informations
        """
        lsblk_info = None
        if not self.mounted:
            logger.warning(f"{self.mountpoint} is unmounted")
        else:
            lsblk_info = Disk.lsblk.get(mountpoint=self.mountpoint)
        fstab_info = Disk.fstab.get(mountpoint=self.mountpoint)
        if not fstab_info:
            logger.warning(f"{self.mountpoint} not found in fstab")
        self.uuid = fstab_info.get("device")
        if not lsblk_info and not self.mounted:
            logger.warning(f"{self.mountpoint} is mounted according to swift but could not find it with lsblk")
        elif not lsblk_info and self.uuid:
            lsblk_info = Disk.lsblk.get(uuid=self.uuid)
        if not lsblk_info:
            logger.warning(f"{self.mountpoint} could not find any matching block device")
            lsblk_info = Disk.lsblk.get(mountpoint=None)
        self.device = lsblk_info.get("path")
        lshw_info = Disk.lshw.get(device=self.device)
        if lshw_info:
            self.size = bytes_to_human_readable(int(lshw_info.get("size"))) if lshw_info.get("size") else None
            self.serial = lshw_info.get("serial")
            self.fw = lshw_info.get("version")
            self.vendor = lshw_info.get("vendor")
            self.model = lshw_info.get("product")
        else:
            self.size = bytes_to_human_readable(int(lsblk_info.get("size"))) if lsblk_info.get("size") else None
            self.serial = lsblk_info.get("serial")
            self.fw = lsblk_info.get("rev")
            self.vendor = lsblk_info.get("vendor")
            self.model = lsblk_info.get("model")
        self.wwn = lsblk_info.get("wwn")
        self.type = "Direct"
        self.slot = ""
        self.state = ""
        if self.wwn and len(Disk.raid.entries) > 0:
            raid_info = Disk.raid.get(serial=self.serial)
            if not len(raid_info):
                raid_info = Disk.raid.get(wwn=self.wwn)
            if len(raid_info):
                self.slot = raid_info['slot']
                self.vendor = raid_info['vendor'].strip()
                self.model = raid_info['model'].strip()
                self.fw = raid_info['rev']
                self.serial = raid_info['serial']
                self.state = f"{raid_info['type']} {raid_info['state']}"
                self.wwn = raid_info['wwn']

    def compare_fw_version(self) -> (int, str):
        """
        Compare disk firmware revbision with minimal revision needed

        Returns:
            int: 0 if firmware is OK, non-zero if update firmware is needed
            str: filename of firmware
        """
        Disk.lsblk = LSblk(logger.level)
        firmware = Disk.lsblk.get(path=self.device, select=True)
        if firmware['rev'] is None:
            Disk.raid = Raid()
            firmware = Disk.raid.get(slot=self.slot)
        with open(conf.get('fw_conf_file')) as f:
            for line in f:
                elem = line.strip().split(',')
                if firmware['model'] == elem[0]:
                    result = subprocess.run(['dpkg', '--compare-versions', firmware['rev'], 'lt', elem[2]],
                                            stdout=subprocess.DEVNULL,
                                            stderr=subprocess.DEVNULL,
                                            capture_output=False,
                                            timeout=3)
                    return result.returncode, elem[3]
        return -1, None

    def update_fw_by_OpenSeaChest(self, fw: str) -> bool:
        """
        Update firmware with OpenSeaChest

        Parameters:
            fw (str): filename of firmware

        Returns:
            bool: True if update is OK
        """
        logger.warning("Try update firmware with openSeaChest_Firmware")
        result = subprocess.run(['openSeaChest_Firmware',
                                 '-d',
                                 self.device,
                                 '--downloadFW',
                                 f"{conf.get('fw_dir')}/{fw}"],
                                stdout=subprocess.DEVNULL,
                                stderr=subprocess.DEVNULL,
                                capture_output=False,
                                timeout=30)
        if result.returncode in [0, 32, 33]:
            print("Update of Firmware OK")
            return True
        else:
            logger.warning("Update with openSeaChest_Firmware failed !")
            logger.warning(result.stderr)
            return False

    def update_fw_by_storcli(self, fw: str, mode: str = '') -> bool:
        """
        Update firmware with storcli64

        Parameters:
            fw (str): filename of firmware
            mode (str): mode of update (see storcli documentation)

        Returns:
            bool: True if update is OK
        """
        return Disk.raid.update_firmware(self.slot, fw, mode)

    def update_fw(self):
        """
        Update firmware
        """
        if args.no_update_firmware:
            return True
        print(f"Update {self.device} firmware...")
        ret, fw = self.compare_fw_version()
        if ret != 0:
            print('Firmware version OK')
        else:
            if not (self.update_fw_by_storcli(fw) or self.update_fw_by_storcli(fw, mode=5) or self.update_fw_by_OpenSeaChest(fw)):
                logger.error(f"Update {self.device} firmware failed !")
                return False
        return True

    def configure_raid0(self) -> bool:
        """
        Configure a Raid0 virtual drive with a phisical Drive

        Returns:
            bool: True if VD correctly created
        """
        if "RAID0" in self.state.upper():
            return Disk.raid.configure_raid0(self.path)
        return True

    def get_uuid(self) -> str:
        """
        Get UUID of a device

        Returns:
            str: Disk UUID
        """
        logger.info(f"Get new UUID of {self.device}")
        Disk.lsblk = LSblk(logger.level)
        return Disk.lsblk.get(path=self.device)['uuid']

    def mkfs(self) -> bool:
        """
        Create FS with mkfs.xfs

        Returns:
            bool: True if FS as correctly created
        """
        print(f"Format {self.device}...")
        result = subprocess.run(['mkfs.xfs',
                                 '-f',
                                 self.device],
                                capture_output=True,
                                timeout=60)
        if result.returncode == 0:
            print("Disk formatting in XFS")
            time.sleep(5)
            return True
        else:
            logger.error(f"Format of {self.device} failed !")
            logger.error(result.stderr)
            return False

    def modif_fstab(self, new_uuid: str) -> bool:
        """
        Modify fstab

        Parameters:
            new_uuid (str): new UUID of device

        Returns:
            bool: True if correctly changed
        """
        print(f"Uncomment and change UUID in fstab, {self.uuid} to {new_uuid}")
        Disk.fstab = FStab(logger.level)
        fstab_entry = Disk.fstab.get(device=self.uuid, select=True)
        fstab_entry['commented'] = False
        fstab_entry['device'] = new_uuid
        Disk.fstab.save()
        try:
            subprocess.run(['systemctl', 'daemon-reload'],
                           stderr=subprocess.DEVNULL,
                           timeout=3)
            time .sleep(1)
            return True
        except Exception as e:
            logger.error(e)
            return False

    def format(self):
        """
        Format Disk and modify fstab
        """
        if args.no_format:
            return True
        if self.mkfs():
            return self.modif_fstab(self.get_uuid())

    def mounting(self) -> bool:
        """
        Mount a disk

        Returns:
            bool: True if disk correctly mounting
        """
        if os.path.ismount(self.mountpoint):
            print("Swift mountpoint is in use")
            return False
        print(f"Mounting {self.mountpoint}")
        result = subprocess.run(['mount', self.mountpoint],
                                capture_output=True,
                                timeout=10)
        if result.returncode == 0:
            return self.wait_mounted()
        else:
            logger.error(f"Mount of {self.mountpoint} failed !")
            logger.error(result.stderr)
            return False

    def wait_mounted(self, timeout: int = 60) -> bool:
        """
        Wait for disk effectively mounted

        Parameters:
            timeout (int): timeout in seconds

        Returns:
            bool: True if disk correctly mounted, False if timeout reached
        """
        cpt = 0
        while not os.path.ismount(self.mountpoint):
            cpt += 1
            time.sleep(1)
            if cpt > timeout:
                print(f"{self.mountpoint} not effectively mounted !")
                logger.error(f"{self.mountpoint} not effectively mounted !")
                return False
        print(f"{self.mountpoint} mounted")
        return True

    def change_owner(self) -> bool:
        """
        Change owner of a mountpoint

        Returns:
            bool: True if change owner is OK
        """
        print(f"Changing disk's owner of {self.mountpoint}")
        uid = pwd.getpwnam('swift')
        try:
            os.chown(self.mountpoint, uid.pw_uid, uid.pw_gid)
            print(f"Owner of {self.mountpoint} is swift:swift")
            stat = os.stat(self.mountpoint)
            if stat.st_uid == uid.pw_uid and stat.st_gid == uid.pw_gid:
                return True
            else:
                print(f"Changing disk's owner of {self.mountpoint} failed !")
                logger.error(f"Changing disk's owner of {self.mountpoint} failed !")
        except Exception as e:
            print(f"Changing disk's owner of {self.mountpoint} failed !")
            logger.error(e)

    def mount(self):
        """
        Mount a disk, if mount and change owner are OK, reload disk informations and save Disks list
        """
        if args.no_mount:
            return True
        if self.mounting():
            if self.change_owner():
                clear_kern_legacy()
                print(f"Disk {self.mountpoint} correctly replace !")
                time.sleep(5)
                return True
        return False

    def replace(self):
        """
        Replace a disk and stop blinking LED locate disk
        """
        if self.update_fw() and self.configure_raid0():
            if self.format():
                if self.mount():
                    self.mounted = True
                    Disk.init_cache()
                    self.load()
                    Disk.save()
                    self.unlocate()
                else:
                    print(f"An error occure when formatting {self.device},\n"
                          f"you can try \'ferreus-orbite replace --no-update_firmware --no-format {self.mountpoint}\'")
            else:
                print(f"An error occure when formatting {self.device},\n"
                      f"you can try \'ferreus-orbite replace --no-update_firmware {self.mountpoint}\'")

    @classmethod
    def get_by_key(cls, **kwargs) -> list:
        """
        Get a disk by a property

        returns:
            list: list of disks
        """
        lookup = []
        for item in Disk.list:
            if any(item.get(key) == value for key, value in kwargs.items()):
                lookup.append(item)
        if not lookup:
            return [Disk(**kwargs)]
        return lookup

    @classmethod
    def get_list_filtered(cls):
        """
        Get a list of disks filtered
        """
        return [disk.get_fildered() for disk in Disk.list]

    @classmethod
    def init_cache(cls):
        """
        Initialize cache
        """
        Disk.recon = SwiftRecon(logger.level)
        Disk.fstab = FStab(logger.level)
        Disk.lsblk = LSblk(logger.level)
        Disk.lshw = LShw(logger.level)
        Disk.raid = Raid()

    @classmethod
    def load_from_file(cls, refresh: bool = False):
        """
        Load information from a json file

        Parameters:
            refresh (bool): If it's necessary to refresh informations
        """
        Disk.recon = SwiftRecon(logger.level)
        if os.path.exists(conf.get('disks_info_file')):
            m_time = os.path.getmtime(conf.get('disks_info_file'))
            dt_m = datetime.datetime.fromtimestamp(m_time)
            logger.info(f"Using cache file {conf.get('disks_info_file')} modified {dt_m}")
            try:
                with open(conf.get('disks_info_file'), 'r') as f:
                    disks = json.load(f)
                    for disk in disks:
                        d = Disk(disk)
                        d.mounted = d.mountpoint.split('/')[-1] not in Disk.recon.unmounted
                        if not d.mounted:
                            if Disk.raid is None:
                                Disk.raid = Raid()
                            d.state = Disk.raid.get_state(d.slot)
            except Exception:
                Disk.load_all()
                return
            if refresh:
                Disk.load_all()
        else:
            Disk.load_all()
            return

    @classmethod
    def load_all(cls):
        """
        Load all disks informations
        """
        Disk.init_cache()
        logger.info(f"swift devices: {Disk.recon.devices}")
        logger.info(f"swift unmounted: {Disk.recon.unmounted}")
        for swift_device in Disk.recon.devices:
            mountpoint = f"{conf.get('mount_dir')}/{swift_device}"
            mounted = swift_device not in Disk.recon.unmounted
            disk = Disk.get_by_key(mountpoint=mountpoint)[0]
            disk.mounted = mounted
            disk.load()
        Disk.save()

    @classmethod
    def print_all(cls, format: str = None):
        """
        Print all disks informations

        Parameters:
            format (str): format of output
        """
        colors = conf.get('colors')
        if len(Disk.list) == 0:
            logger.warning("No disks found")
            return
        if 'mountpoint' not in Disk.field_names:
            Disk.field_names.insert(0, 'mountpoint')
        if 'mounted' not in Disk.field_names:
            Disk.field_names.append('mounted')
        disks = Disk.get_list_filtered()
        if format == 'json':
            print(json.dumps(disks, indent=4))
        elif format == 'yaml':
            print(yaml.dump(disks, sort_keys=False))
        elif format == 'csv':
            writer = csv.DictWriter(sys.stdout, fieldnames=Disk.field_names)
            writer.writeheader()
            writer.writerows(disks)
        elif format == 'textile':
            fn = Disk.field_names.copy()
            any_unmounted = 0
            for f in fn:
                print(f"|_.{f.upper()}", end='')
            print('|')
            for disk in disks:
                if not disk['mounted']:
                    any_unmounted += 1
                    print(f"{colors['textile']['red']}", end='')
                for f in fn:
                    print(f"|{disk[f]}", end='')
                print('|')
            if any_unmounted > 1:
                print("WARNING : for disks unmounted (RED), mapping between device and mountpoint isn't sure !")
        else:
            fn = [word.upper() for word in Disk.field_names]
            any_unmounted = 0
            ptable = prettytable.PrettyTable()
            ptable.field_names = fn
            for disk in disks:
                if not disk['mounted']:
                    any_unmounted += 1
                    row = {key: f"{colors['term']['red']}{value}{colors['term']['neutral']}" for key, value in disk.items()}
                else:
                    row = disk
                ptable.add_row(row.values())
                try:
                    ptable.set_style(prettytable.PLAIN_COLUMNS)
                except Exception:
                    pass
            print(ptable.get_string(sortby='MOUNTPOINT', fields=fn))
            if any_unmounted > 1:
                print("WARNING : for disks unmounted (RED), mapping between device and mountpoint isn't sure !")

    @classmethod
    def save(cls):
        """
        Save disks informations to a json file
        """
        with open(conf.get('disks_info_file'), 'w') as f:
            json.dump(Disk.list, f, indent=4)


def test_filter(string: str) -> str:
    """
    Test validity of a filter argument

    Parameters:
        string (str): argument to test

    Returns:
        str: valid arguments

    Raises:
        argparse.ArgumentTypeError: raise error if isn't a valid argument
    """
    path = os.path.join(conf.get('mount_dir'), string)
    if os.path.isdir(path) or os.path.exists(string) or len(string.split('=')) == 2:
        return string
    else:
        raise argparse.ArgumentTypeError(f"Filter {string} not valid !")


def parse_args() -> argparse.Namespace:
    """
    Prepare arguments off application

    Returns:
        argparse.Namespace: the namespace off this argument parser
    """
    parser = argparse.ArgumentParser(description="Utility to retrieve information from physical disks")
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Number of repeat increase log level")
    parser.add_argument('-f', '--format', choices=['json', 'yaml', 'textile', 'csv'], help='Output format (textile is for redmine)')
    parser.add_argument('-r', '--refresh', action='store_true', help='Import from file')
    subparsers = parser.add_subparsers(help="Sub-command help", required=True, dest='action')
    parser_list = subparsers.add_parser('list', help="List disk info")
    parser_list.set_defaults(func=fo_list)
    parser_list.add_argument('-o', '--output', help=f"Fields to output ({','.join(Disk.field_names)}), separate by comma. 'mountpoint' and 'mounted automatically added")
    parser_info = subparsers.add_parser('info', help="Display information about one disk")
    parser_info.set_defaults(func=fo_info)
    group = parser_info.add_mutually_exclusive_group()
    group.add_argument('-l', '--locate', action='store_true', help='Locate (start blinking) disk')
    group.add_argument('-u', '--unlocate', action='store_true', help='Unlocate (stop blinking) disk')
    parser_info.add_argument('filter', type=test_filter, help=f"Filter by mountpoint (without {conf.get('mount_dir')}/), device(/dev/sdX) or by key=value")
    parser_info.add_argument('-o', '--output', help=f"Fields to output ({','.join(Disk.field_names)}), separate by comma")
    parser_replace = subparsers.add_parser('replace', help="Replace a disk")
    parser_replace.set_defaults(func=fo_replace)
    parser_replace.add_argument('filter', type=test_filter, help=f"Filter by mountpoint (without {conf.get('mount_dir')}/), device(/dev/sdX) or by key=value")
    parser_replace.add_argument('-U', '--no-update_firmware', action='store_true', help='Don\'t update firmware of disk')
    parser_replace.add_argument('-F', '--no-format', action='store_true', help='Don\'t format disk in Xfs')
    parser_replace.add_argument('-M', '--no-mount', action='store_true', help='Don\'t mount disk')
    parser_detail = subparsers.add_parser('detail', help="Details for system command")
    parser_detail.set_defaults(func=fo_detail)
    parser_detail.add_argument('filter', choices=['raid', 'lsblk', 'lshw', 'swift', 'fstab'], help="Command to return details")
    try:
        argcomplete.autocomplete(parser)
    except Exception:
        pass
    return parser.parse_args()


def setup_logging(verbosity) -> logging.Logger:
    """
    Configure the logger

    Returns:
        logging.Logger: the logger
    """
    format = "%(levelname)s: %(message)s"
    verbosity = verbosity if verbosity < 5 else 4
    level = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]
    level.reverse()
    logging.basicConfig(format=format)
    logger = logging.getLogger("ferreus_orbit")
    logger.setLevel(level[verbosity])
    return logger


def fo_list():
    """
    Callback function for list subcommand
    """
    Disk.load_from_file(args.refresh)
    if 'output' in args and args.output:
        Disk.field_names = args.output.split(',')
    Disk.print_all(args.format)


def fo_info():
    """
    Callback function for info subcommand
    """
    Disk.load_from_file(args.refresh)
    if 'output' in args and args.output:
        Disk.field_names = args.output.split(',')
    path = os.path.join(conf.get('mount_dir'), args.filter)
    if os.path.isdir(path):
        disks = Disk.get_by_key(mountpoint=path)
    elif os.path.exists(args.filter):
        disks = Disk.get_by_key(device=args.filter)
    else:
        filter = args.filter.split('=')
        arg = {filter[0].lower(): filter[1]}
        disks = Disk.get_by_key(**arg)
    for disk in disks:
        disk.print(args.format)
        if args.locate:
            disk.locate(args.format)
        elif args.unlocate:
            disk.unlocate(args.format)


def fo_replace():
    """
    Callback function for replace subcommand
    """
    Disk.load_from_file(args.refresh)
    path = os.path.join(conf.get('mount_dir'), args.filter)
    if os.path.isdir(path):
        disks = Disk.get_by_key(mountpoint=path)
    elif os.path.exists(args.filter):
        disks = Disk.get_by_key(device=args.filter)
    else:
        filter = args.filter.split('=')
        arg = {filter[0].lower(): filter[1]}
        disks = Disk.get_by_key(**arg)
    if len(disks) > 1:
        li = {}
        for disk in disks:
            li[f"{disk.mountpoint}, {disk.device} (SN: {disk.serial})"] = disk
        disk = selectFromDict(li, " Select correct device")
    elif len(disks) == 0:
        print(f"No device matching with {args.filter}")
        logger.warning(f"No device matching with {args.filter}")
        return
    else:
        disk = disks[0]
    disk.replace()


def fo_detail():
    """
    Callback function for detail subcommand
    """
    if args.filter == 'raid':
        list = Raid(logger.level).detail()
    elif args.filter == 'lsblk':
        list = LSblk(logger.level).detail()
    elif args.filter == 'lshw':
        list = LShw(logger.level).detail()
    elif args.filter == 'swift':
        list = SwiftRecon(logger.level).detail()
    elif args.filter == 'fstab':
        list = FStab(logger.level).detail()
    fn = []
    for ls in list:
        if isinstance(ls, dict):
            for i in ls.keys():
                if i not in fn:
                    fn.append(i)
    if args.format == 'json':
        print(json.dumps(list, indent=4))
    elif args.format == 'yaml':
        print(yaml.dump(list, default_flow_style=False))
    elif args.format == 'csv':
        writer = csv.DictWriter(sys.stdout, fieldnames=fn)
        writer.writeheader()
        writer.writerows(list)
    elif args.format == 'textile':
        for f in fn:
            print(f"|_.{f.upper()}", end='')
        print('|')
        for ls in list:
            for f in fn:
                print(f"|{ls[f] if f in ls else ''}", end='')
            print('|')
    else:
        fn = [word.upper() for word in fn]
        ptable = prettytable.PrettyTable()
        ptable.field_names = fn
        for ls in list:
            row = []
            for f in fn:
                if f.lower() in ls:
                    row.append(ls[f.lower()])
                else:
                    row.append('')
            ptable.add_row(row)
        try:
            ptable.set_style(prettytable.PLAIN_COLUMNS)
        except Exception:
            pass
        print(ptable.get_string(fields=fn))


def main():
    global conf, args, logger
    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
    conf = Config()
    args = parse_args()
    logger = setup_logging(args.verbose)
    args.func()


if __name__ == "__main__":
    main()
