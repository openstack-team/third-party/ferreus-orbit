#!/usr/bin/python3

import subprocess
from ferreus_orbit.systemtool import SystemTool
from ferreus_orbit.utils import selectFromDict


class FStab(SystemTool):
    """
    Class for retrieve informations of fstab file
    """
    select = 0

    def run(self) -> list:
        """
        Get informations of fstab file

        Returns:
            list: list of mountpoint
        """
        self.logger.info("Retrieve fstab infos")
        data = []
        with open("/etc/fstab", "r") as file:
            for line in file:
                fstab = {'commented': False}
                if line.startswith('#'):
                    line = line[1:]
                    fstab['commented'] = True
                fields = line.split()
                try:
                    if len(fields) == 6:
                        dev = fields[0].split('=')
                        if len(dev) == 1:
                            fstab['device'] = dev[0]
                        elif len(dev) == 2:
                            fstab['uuid'] = dev[0]
                            fstab['device'] = dev[1]
                        fstab['mountpoint'] = fields[1]
                        fstab['fs'] = fields[2]
                        fstab['options'] = fields[3].split(',')
                        fstab['d'] = int(fields[4])
                        fstab['p'] = int(fields[5])
                        data.append(fstab)
                except Exception:
                    pass
        return data

    def get(self, select: bool = False, **kwargs) -> dict:
        """
        Return one item from entries matching arbitrary keyword arguments

        Parameters:
            select (bool): if selection mode

        Returns:
            dict: dict of device
        """
        lookup = []
        result = {}
        for item in self.entries:
            if any(item.get(key) == value for key, value in kwargs.items()):
                lookup.append(item)
        if not lookup or len(lookup) > 1:
            self.logger.info(f"{self.__class__.__name__} found no entry matching '{kwargs}'or more than 1 entries found")
            if select:
                list_disks = {}
                for disk in self.entries:
                    if disk['commented']:
                        list_disks[f"{disk['mountpoint']} with old UUID: {disk['device']}"] = disk
                result = selectFromDict(list_disks, 'Mountpoint')
            else:
                result = lookup[FStab.select]
                FStab.select += 1
        else:
            result = lookup[0]
        return result

    def save(self):
        """
        Save FStab.entries to /etc/fstab
        And backup old fstab in /etc/fstab.bak

        Raises:
            Exception
        """
        subprocess.call(['cp', '/etc/fstab', '/etc/fstab.bak'])
        try:
            with open("/etc/fstab", "w") as file:
                file.write('# This file was modified by ferreus-orbit\n')
                file.write('# a backup file was create in /etc/fstab.bak\n')
                file.write('\n')
                for entry in self.entries:
                    comment = '#' if entry['commented'] else ''
                    device = f"{entry['uuid'].upper()}={entry['device']}" if 'uuid' in entry else entry['device']
                    options = ','.join(entry['options'])
                    file.write(f"{comment}{device}\t{entry['mountpoint']}\t{entry['fs']}\t{options}\t{entry['d']}\t{entry['p']}\n")
        except Exception as e:
            print("fstab file writing error, restore backup file")
            subprocess.call(['cp', '/etc/fstab.bak', '/etc/fstab'])
            raise Exception(e)
