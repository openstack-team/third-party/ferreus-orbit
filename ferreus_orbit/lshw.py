#!/usr/bin/python3

import subprocess
from xml.etree.ElementTree import fromstring
from ferreus_orbit.systemtool import SystemTool


class LShw(SystemTool):
    """
    Class for retrieve informations by lshw command
    """
    select = 0

    def run(self) -> list:
        """
        Get informations by lshw command

        Returns:
            list: list of device
        """
        self.logger.info("Retrieve lshw infos")
        cmd = "lshw -c disk -xml".split()
        result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=30)
        hws = fromstring(result.stdout)
        disks = []
        for hw in hws:
            disk = {}
            disk['vendor'] = hw.findtext('vendor', '')
            disk['product'] = hw.findtext('product', '')
            disk['serial'] = hw.findtext('serial', '')
            disk['size'] = hw.findtext('size', '')
            disk['version'] = hw.findtext('version', '')
            for ln in hw.findall('logicalname'):
                name = ln.text
                if name.startswith('/dev'):
                    disk['device'] = name
                elif name.startswith('/srv/node'):
                    disk['mountpoint'] = name
            disks.append(disk)
        return disks

    def get(self, select: bool = False, **kwargs) -> dict:
        """
        Return one item from entries matching arbitrary keyword arguments

        Parameters:
            select (bool): if selection mode

        Returns:
            dict: dict of device
        """
        lookup = []
        result = {}
        for item in self.entries:
            if any(item.get(key) == value for key, value in kwargs.items()):
                lookup.append(item)
        if not lookup:
            self.logger.info(f"{self.__class__.__name__} found no entry matching '{kwargs}'")
        elif len(lookup) > 1:
            self.logger.warning(f"{self.__class__.__name__} found more than 1 disk found for '{kwargs}', result: {lookup}")
        else:
            result = lookup[0]
        return result
