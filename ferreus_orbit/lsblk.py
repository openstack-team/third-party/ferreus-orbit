#!/usr/bin/python3

import subprocess
import json
from ferreus_orbit.systemtool import SystemTool
from ferreus_orbit.utils import selectFromDict


class LSblk(SystemTool):
    """
    Class for retrieve informations by lsblk command
    """
    select = 0

    def run(self) -> list:
        """
        Get informations by lsblk command

        Returns:
            list: list of device
        """
        self.logger.info("Retrieve lsblk infos")
        cmd = "lsblk --json --bytes -T --output MOUNTPOINT,UUID,PATH,SIZE,SERIAL,REV,WWN,VENDOR,MODEL".split()
        result = subprocess.run(cmd, capture_output=True, check=True, text=True, timeout=3)
        disks = [disk for disk in json.loads(result.stdout).get("blockdevices") if 'children' not in disk and disk['size'] > 10000000000000]
        return disks

    def get(self, select: bool = False, **kwargs) -> dict:
        """
        Return one item from entries matching arbitrary keyword arguments

        Parameters:
            select (bool): if selection mode

        Returns:
            dict: dict of device
        """
        lookup = []
        result = {}
        for item in self.entries:
            if any(item.get(key) == value for key, value in kwargs.items()):
                lookup.append(item)
        if not lookup:
            self.logger.info(f"{self.__class__.__name__} found no entry matching '{kwargs}'")
        elif len(lookup) > 1:
            self.logger.warning(f"{self.__class__.__name__} found more than 1 disk found for '{kwargs}', result: {lookup}")
            if select:
                list_disks = {}
                for disk in lookup:
                    list_disks[f"{disk['path']}: {disk['vendor']} {disk['model']}, Serial {disk['serial']}"] = disk
                result = selectFromDict(list_disks, 'Disk')
            else:
                result = lookup[LSblk.select]
                LSblk.select += 1
        else:
            result = lookup[0]
        return result
