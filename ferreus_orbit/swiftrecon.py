#!/usr/bin/python3

import socket
import requests
import logging
from ferreus_orbit.config import Config

conf = Config()


class SwiftRecon:
    """
    Class for retrieve informations with swift recon request
    """

    def __init__(self, level=logging.CRITICAL):
        """
        Get informations with a swift recon request
        """
        hostname = socket.getfqdn()
        self._recon = f"http://{hostname}:6200/recon"
        self.unmounted = self.get_unmounted()
        self.devices = self.get_devices()
        self.logger = logging.getLogger(f"ferreus_orbit_{self.__class__.__name__}")
        self.logger.setLevel(level)

    def get_unmounted(self) -> list:
        """
        Get unmounted swift device

        Returns:
            list: list of unmounted swift device
        """
        response = requests.get(f"{self._recon}/unmounted", timeout=3)
        response.raise_for_status()
        return [x["device"] for x in response.json() if x["mounted"] is False]

    def get_devices(self) -> list:
        """
        Get list of swift device

        Return:
            list: list of swift device
        """
        response = requests.get(f"{self._recon}/devices", timeout=3)
        response.raise_for_status()
        data = response.json()
        if len(data) > 1:
            self.logger.warning("more than 1 key in response")
        if conf.get('mount_dir') not in data:
            self.logger.warning(f"{conf.get('mount_dir')} not in response")
        return sorted(data[conf.get('mount_dir')])

    def detail(self) -> list:
        """
        Return contents of devices

        Retrun: list of devices mounted or not
        """
        rows = []
        for dev in self.devices:
            row = {'swift mountpoint': f"{conf.get('mount_dir')}/{dev}", 'unmounted': True if dev in self.unmounted else False}
            rows.append(row)
        return rows
